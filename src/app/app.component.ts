import {Component, OnInit} from '@angular/core';
import {AuthService} from './core/auth.service';
import {ActivatedRoute, Router} from '@angular/router';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.less']
})
export class AppComponent implements OnInit{
  title = 'Admin TAXI';

  constructor() {

  }

  ngOnInit(): void {

  }
}
