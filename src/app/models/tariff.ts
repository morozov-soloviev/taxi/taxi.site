export class Tariff {
    id: string;
    name: string;
    rank: number;
    distancePrice: number;
    timePrice: number;
}
