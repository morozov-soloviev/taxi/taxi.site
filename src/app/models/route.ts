export class Route {
    country: string;
    region: string;
    city: string;
    street: string;
    buildingNumber: string;
    specificAddress: string;
    latitude: number;
    longitude: number;
}
