import {Driver} from './driver';
import {Car} from './car';

export class Board {
    id: string;
    latitude: number;
    longitude: number;
    car: Car;
    driver: Driver
}
