export class User {
    id: string;
    firstName: string;
    lastName: string;
    middleName: string;
    gender: boolean;
    phone: string;
    email: string;
}
