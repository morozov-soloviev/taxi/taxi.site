export class Driver {
    id: string;
    userId: string;
    firstName: string;
    lastName: string;
    middleName: string;
    gender: 0;
    phone: string;
    email: string;
    isActive: boolean;
}
