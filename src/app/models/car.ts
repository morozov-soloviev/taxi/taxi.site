export class Car {
    id: string;
    ownerId: string;
    mark: string;
    model: string;
    type: {
        id: string;
        name: string
    };
    options: [
        {
            id: string;
            name: string
        }
    ];
    number: string;
    year: string;
    carTypeId: string;
    carOptionsIds: string[];
    tarifRank: number;

}

export class CarType {
    id: string;
    name: string;
}

export class CarOption {
    id: string;
    name: string;
}
