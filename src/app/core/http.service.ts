import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders, HttpParams} from '@angular/common/http';
import {AuthService} from './auth.service';
import {ActivatedRoute, Router} from '@angular/router';
import {Observable} from 'rxjs';
import {Car} from '../models';


@Injectable({
    providedIn: 'root'
})


export class HttpService {

    // readonly api = 'api/';

    httpOptions = {
        headers: new HttpHeaders({
            'Content-Type': 'application/json',
            'Authorization': 'Bearer ' + localStorage.getItem('token')
        })
    };

    constructor(private http: HttpClient,
                private authService: AuthService,
                private router: Router) {
        this.authService.token.subscribe((value => {
            if (value) {
                localStorage.setItem('token', value);
                console.log(this.httpOptions.headers.get('Authorization'));
                console.log(value);


                this.httpOptions.headers = new HttpHeaders(
                    {
                        'Content-Type': 'application/json',
                        'Authorization': 'Bearer ' + value,
                    });
                console.log(this.httpOptions.headers.get('Authorization'));
                this.router.navigate(['root']);
            } else {
                this.router.navigate(['login']);
            }
        }));
    }


    login(data) {
        return this.http.post('user/login', data, this.httpOptions);
    }

    getUsers(): Observable<Object> {
        // debugger;

        console.log(this.httpOptions.headers.get('Authorization'));

        return this.http.get('data/user/list', this.httpOptions);
    }

    getUser(id: string) {
        return this.http.post('data/user/info', {
            id: id
        }, this.httpOptions);

    }

    getDrivers(): Observable<Object> {
        return this.http.get('data/driver/list', this.httpOptions);
    }

    getDriver(id: string) {
        return this.http.post('data/driver/info', {
            id: id
        }, this.httpOptions);

    }

    activeDriver(id, bool) {
        return this.http.post('data/driver/save', {
            id: id,
            isActive: bool
        }, this.httpOptions);
    }


    getTariffs(): Observable<Object> {
        return this.http.get('data/tarif/list', this.httpOptions);
    }

    getTariff(id: string) {
        return this.http.post('data/tarif/info', {
            id: id
        }, this.httpOptions);

    }

    getOrders(): Observable<Object> {
        return this.http.get('data/order/list', this.httpOptions);
    }

    getOrder(id: string) {
        return this.http.post('data/order/info', {
            id: id
        }, this.httpOptions);

    }

    getCars(): Observable<Object> {
        return this.http.get('data/car/list', this.httpOptions);
    }

    getCar(id: string) {
        return this.http.post('data/car/info', {
            id: id
        }, this.httpOptions);

    }

    postCar(car: Car) {
        return this.http.post('data/car/save', {
            'ownerId': car.ownerId,
            'mark': car.mark,
            'model': car.model,
            'carTypeId': car.carTypeId,
            'carOptionsIds': car.carOptionsIds,
            'number': car.number,
            'year': '2019-12-15T15:28:25Z',
            'tarifRank': parseInt(String(car.tarifRank))
        }, this.httpOptions);
    }

    // carTypeId
    // id: "a245f9a1-fa31-4552-bf88-4e4689a0b861"
    // name: "Хетчбек"

    // carOptionsIds
    // id: "b98ab410-bdce-4aea-9300-48f41371873c"
    // name: "Детское сиденье"
    // id: "4166cfc2-46b2-44a3-9fa2-c21b4d0f12d8"
    // name: "Доставка"

    getCarOptions(): Observable<Object> {
        return this.http.get('data/carOption/list', this.httpOptions);
    }

    getCarOption(id: string) {
        return this.http.post('data/carOption/info', {
            id: id
        }, this.httpOptions);

    }

    getCarTypes(): Observable<Object> {
        return this.http.get('data/carType/list', this.httpOptions);
    }

    getCarType(id: string) {
        return this.http.post('data/carType/info', {
            id: id
        }, this.httpOptions);

    }


    // getDrivers(): Observable<Object>{
    //     return this.http.get('data/driver/list', this.httpOptions);
    // }
    //
    // getDriver(id: string) {
    //     return this.http.post('data/driver/info', {
    //         id: id
    //     }, this.httpOptions);
    //
    // }

}
