import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';

import {AppRoutingModule} from './app-routing.module';
import {AppComponent} from './app.component';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {HttpClientModule} from '@angular/common/http';
import {RouterModule, Routes} from '@angular/router';
import {LoginComponent} from './modules/login/login/login.component';
import {LoginModule} from './modules/login/login.module';
import {MatInputModule} from '@angular/material/input';
import {RootComponent} from './modules/display/root/root.component';
import {DisplayModule} from './modules/display/display.module';
import {UsersComponent} from './modules/display/components/users/users.component';
import {DriversComponent} from './modules/display/components/drivers/drivers.component';
import {MatTableModule} from '@angular/material/table';
import {DriverComponent} from './modules/display/components/driver/driver.component';
import {UserComponent} from './modules/display/components/user/user.component';
import {TariffsComponent} from './modules/display/components/tariffs/tariffs.component';
import {OrdersComponent} from './modules/display/components/orders/orders.component';
import {OrderComponent} from './modules/display/components/order/order.component';
import {CarsComponent} from './modules/display/components/cars/cars.component';
import {CarTypesComponent} from './modules/display/components/carTypes/car-types.component';
import {CarOptionsComponent} from './modules/display/components/carOptions/car-options.component';

const appRoutes: Routes = [
    {path: 'login', component: LoginComponent},
    {
        path: 'root', component: RootComponent,
        children: [
            {path: 'users', component: UsersComponent},
            {path: 'user/:id', component: UserComponent},
            {path: 'drivers', component: DriversComponent},
            {path: 'driver/:id', component: DriverComponent},
            {path: 'tariffs', component: TariffsComponent},
            {path: 'tariffs/:id', component: DriverComponent},
            {path: 'orders', component: OrdersComponent},
            {path: 'order/:id', component: OrderComponent},
            {path: 'cars', component: CarsComponent},
            {path: 'car/:id', component: OrderComponent},
            {path: 'car-types', component: CarTypesComponent},
            {path: 'car-type/:id', component: OrderComponent},
            {path: 'car-options', component: CarOptionsComponent},
            {path: 'car-option/:id', component: OrderComponent},

            // {path: 'drivers', component: DriverComponent},
            {
                path: '',
                redirectTo: 'users',
                pathMatch: 'full'
            },
        ]
    },

    // { path: 'hero/:id',      component: HeroDetailComponent },
    // {
    //   path: 'heroes',
    //   component: HeroListComponent,
    //   data: { title: 'Heroes List' }
    // },
    {
        path: '',
        redirectTo: '/login',
        pathMatch: 'full'
    },
    // { path: '**', component: PageNotFoundComponent }
];

@NgModule({
    declarations: [
        AppComponent
    ],
    imports: [
        BrowserModule,
        AppRoutingModule,
        BrowserAnimationsModule,
        HttpClientModule,
        RouterModule.forRoot(appRoutes),
        MatInputModule,
        MatTableModule,
        LoginModule,
        DisplayModule,
    ],
    providers: [],
    bootstrap: [AppComponent]
})
export class AppModule {
}
