import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {UsersComponent} from './components/users/users.component';
import {DriversComponent} from './components/drivers/drivers.component';
import {RootComponent} from './root/root.component';
import {MatToolbarModule} from '@angular/material/toolbar';
import {RouterModule, Routes} from '@angular/router';
import { DriverComponent } from './components/driver/driver.component';
import { UserComponent } from './components/user/user.component';
import {MatTableModule} from '@angular/material/table';
import {MatCardModule} from '@angular/material/card';
import {MatListModule} from '@angular/material/list';
import {MatButtonModule} from '@angular/material/button';
import {MatMenuModule} from '@angular/material/menu';
import {MatIconModule} from '@angular/material/icon';
import {TariffsComponent} from './components/tariffs/tariffs.component';
import {OrdersComponent} from './components/orders/orders.component';
import {OrderComponent} from './components/order/order.component';
import {CarTypesComponent} from './components/carTypes/car-types.component';
import {CarsComponent} from './components/cars/cars.component';
import {CarOptionsComponent} from './components/carOptions/car-options.component';
import {MatFormFieldModule} from '@angular/material/form-field';
import {MatSelectModule} from '@angular/material/select';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {MatInputModule} from '@angular/material/input';

const displayRoutes: Routes = [


    // { path: '**', component: PageNotFoundComponent }
];

@NgModule({
    declarations: [UsersComponent,
        DriversComponent,
        RootComponent,
        DriverComponent,
        UserComponent,
        TariffsComponent,
        OrdersComponent,
        OrderComponent,
        CarsComponent,
        CarTypesComponent,
        CarOptionsComponent,
    ],
    imports: [
        CommonModule,
        MatToolbarModule,
        RouterModule.forRoot(displayRoutes),
        MatTableModule,
        MatCardModule,
        MatListModule,
        MatButtonModule,
        MatMenuModule,
        MatIconModule,
        MatFormFieldModule,
        MatSelectModule,
        ReactiveFormsModule,
        MatInputModule,
        FormsModule,
    ],
    exports: [RootComponent]
})
export class DisplayModule {
}
