import {Component, OnInit} from '@angular/core';
import {Tariff, User} from '../../../../models';
import {HttpService} from '../../../../core/http.service';
import {AuthService} from '../../../../core/auth.service';
import {ActivatedRoute, Router} from '@angular/router';


@Component({
    selector: 'app-tariffs',
    templateUrl: './tariffs.component.html',
    styleUrls: ['./tariffs.component.less']
})
export class TariffsComponent implements OnInit {

    displayedColumns: string[] = ['name', 'distancePrice', 'timePrice'];

    tariffs: Tariff[];


    constructor(private httpService: HttpService,
                private authService: AuthService,
                private router: Router,
                private route: ActivatedRoute,
    ) {
    }

    ngOnInit() {
        this.httpService.getTariffs().subscribe((res) => {
            this.tariffs = res['data'].tarifs;
            console.log(this.tariffs);
        });
    }

    goToUser(row: any) {
        console.log(row);
        this.router.navigate(['user', row.id], {relativeTo: this.route.parent});
    }
}
