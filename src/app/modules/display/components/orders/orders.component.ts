import {Component, OnInit} from '@angular/core';
import {Tariff, User, Order} from '../../../../models';
import {HttpService} from '../../../../core/http.service';
import {AuthService} from '../../../../core/auth.service';
import {ActivatedRoute, Router} from '@angular/router';


@Component({
    selector: 'app-orders',
    templateUrl: './orders.component.html',
    styleUrls: ['./orders.component.less']
})
export class OrdersComponent implements OnInit {

    displayedColumns: string[] = ['start',
        'calculatedAmount',
        'status',
        'tariffName',
    ];

    orders: Order[];


    constructor(private httpService: HttpService,
                private authService: AuthService,
                private router: Router,
                private route: ActivatedRoute,
    ) {
    }

    ngOnInit() {
        this.httpService.getOrders().subscribe((res) => {
            this.orders = res['data'].orders;
            console.log(this.orders);
        });
    }

    goToUser(row: any) {
        console.log(row);
        this.router.navigate(['order', row.id], {relativeTo: this.route.parent});
    }
}
