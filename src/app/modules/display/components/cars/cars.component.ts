import {Component, OnInit} from '@angular/core';
import {Car, Tariff, User} from '../../../../models';
import {HttpService} from '../../../../core/http.service';
import {AuthService} from '../../../../core/auth.service';
import {ActivatedRoute, Router} from '@angular/router';


@Component({
    selector: 'app-tariffs',
    templateUrl: './cars.component.html',
    styleUrls: ['./cars.component.less']
})
export class CarsComponent implements OnInit {

    displayedColumns: string[] = ['mark', 'model', 'number'];

    cars: Car[];

    constructor(private httpService: HttpService,
                private authService: AuthService,
                private router: Router,
                private route: ActivatedRoute,
    ) {
    }

    ngOnInit() {
        this.httpService.getCars().subscribe((res) => {
            this.cars = res['data'].cars;
            console.log(this.cars);
        });

    }

    goToUser(row: any) {
        console.log(row);
        this.router.navigate(['user', row.id], {relativeTo: this.route.parent});
    }
}
