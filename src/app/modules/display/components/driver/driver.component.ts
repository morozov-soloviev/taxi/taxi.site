import {Component, OnInit} from '@angular/core';
import {Car, CarOption, CarType, Driver, User} from '../../../../models';
import {HttpService} from '../../../../core/http.service';
import {ActivatedRoute, Router} from '@angular/router';
import {FormBuilder, FormGroup} from '@angular/forms';

@Component({
    selector: 'app-driver',
    templateUrl: './driver.component.html',
    styleUrls: ['./driver.component.less']
})
export class DriverComponent implements OnInit {

    displayedColumns: string[] = ['mark', 'model', 'number'];

    isOpenFormForCar = false;
    closeTable = false;

    driver: Driver;
    car: Car;
    carTypes: CarType[];
    carOptions: CarOption[];
    cars: Car[];

    options: FormGroup;


    constructor(private httpService: HttpService,
                private router: Router,
                private route: ActivatedRoute,
                private fb: FormBuilder) {
        this.options = fb.group({
            hideRequired: false,
            floatLabel: 'auto',
        });
    }

    ngOnInit() {
        console.log(this.route.snapshot.params);
        this.httpService.getDriver(this.route.snapshot.params['id'])
            .subscribe((res) => {
                this.driver = res['data'].driver;
                this.httpService.getUser(this.driver.userId)
                    .subscribe((res) => {
                        this.driver = {
                            ...this.driver,
                            ...res['data'].user.contact
                        };

                        this.httpService.getCars().subscribe((res) => {
                            this.cars = res['data'].cars.filter( (car) => car.owner.id === this.driver.userId);
                            console.log(this.cars);
                        });

                        console.log(this.driver);
                    });
            });
        this.httpService.getCarTypes().subscribe((res) => {
            this.carTypes = res['data'].carTypes;
            console.log('carTypes', this.carTypes);
        });
        this.httpService.getCarOptions().subscribe((res) => {
            this.carOptions = res['data'].carOptions;
            console.log('carOptions', this.carOptions);
        });

    }


    back() {
        this.router.navigate(['drivers'], {relativeTo: this.route.parent});
    }




    toActive() {
        this.httpService.activeDriver(this.route.snapshot.params['id'],!this.driver.isActive ).subscribe(() => {
            this.driver.isActive = !this.driver.isActive;
        });
    }

    openForm() {
        this.car = new Car();
        this.car.ownerId = this.driver.userId;
        this.isOpenFormForCar = true;
    }

    createCar() {
        console.log(this.car);
        this.httpService.postCar(this.car).subscribe((res) => {
            this.closeTable = true;
            this.httpService.getCars().subscribe((res) => {
                this.cars = res['data'].cars.filter( (car) => car.owner.id === this.driver.userId);
                console.log(this.cars);
            });
        });
    }
}
