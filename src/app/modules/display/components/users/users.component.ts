import {Component, OnInit} from '@angular/core';
import {User} from '../../../../models';
import {HttpService} from '../../../../core/http.service';
import {AuthService} from '../../../../core/auth.service';
import {ActivatedRoute, Router} from '@angular/router';

@Component({
    selector: 'app-users',
    templateUrl: './users.component.html',
    styleUrls: ['./users.component.less']
})
export class UsersComponent implements OnInit {

    displayedColumns: string[] = ['firstName', 'lastName', 'middleName', 'phone'];

    users: User[];

    constructor(private httpService: HttpService,
                private authService: AuthService,
                private router: Router,
                private route: ActivatedRoute,
    ) {
    }

    ngOnInit() {
        this.httpService.getUsers().subscribe((res) => {
            this.users = res['data'].users;
            console.log(this.users);
        });
    }

    goToUser(row: any) {
        console.log(row);
        this.router.navigate(['user', row.id], {relativeTo: this.route.parent});
    }
}
