import {Component, OnInit} from '@angular/core';
import {CarOption, CarType, Tariff, User} from '../../../../models';
import {HttpService} from '../../../../core/http.service';
import {AuthService} from '../../../../core/auth.service';
import {ActivatedRoute, Router} from '@angular/router';


@Component({
    selector: 'app-tariffs',
    templateUrl: './car-types.component.html',
    styleUrls: ['./car-types.component.less']
})
export class CarTypesComponent implements OnInit {

    displayedColumns: string[] = ['name'];

    carTypes: CarType[];

    constructor(private httpService: HttpService,
                private authService: AuthService,
                private router: Router,
                private route: ActivatedRoute,
    ) {
    }

    ngOnInit() {
        this.httpService.getCarTypes().subscribe((res) => {
            this.carTypes = res['data'].carTypes;
            console.log(this.carTypes);
        });
    }

    goToUser(row: any) {
        console.log(row);
        this.router.navigate(['user', row.id], {relativeTo: this.route.parent});
    }
}
