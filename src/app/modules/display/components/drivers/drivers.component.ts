import {Component, OnInit} from '@angular/core';
import {HttpService} from '../../../../core/http.service';
import {AuthService} from '../../../../core/auth.service';
import {ActivatedRoute, Router} from '@angular/router';
import {Driver, User} from '../../../../models';
import {forkJoin, Observable} from 'rxjs';

@Component({
    selector: 'app-drivers',
    templateUrl: './drivers.component.html',
    styleUrls: ['./drivers.component.less']
})
export class DriversComponent implements OnInit {

    displayedColumns: string[] = ['firstName', 'lastName', 'middleName', 'phone'];

    drivers: Driver[];

    constructor(private httpService: HttpService,
                private authService: AuthService,
                private router: Router,
                private route: ActivatedRoute,) {
    }

    ngOnInit() {
        forkJoin([
            this.httpService.getDrivers(),
            this.httpService.getUsers()
        ]).subscribe((res) => {
            this.drivers = res[0]['data'].drivers;
            const users = res[1]['data'].users;
            this.drivers = this.drivers.map(driver => {
                const user = users.find(u => u.id === driver.userId);
                console.log({
                  ...user,
                  ...driver,
                });
                return {
                    ...user,
                    ...driver,

                }
            });

            console.log(this.drivers);
        });
    }

    goToUser(row: any) {
        this.router.navigate(['driver', row.id], {relativeTo: this.route.parent});
    }

}
