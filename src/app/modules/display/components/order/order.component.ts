import {Component, OnInit} from '@angular/core';
import {Order, User} from '../../../../models';
import {HttpService} from '../../../../core/http.service';
import {ActivatedRoute, Router} from '@angular/router';

@Component({
    selector: 'app-user',
    templateUrl: './order.component.html',
    styleUrls: ['./order.component.less']
})
export class OrderComponent implements OnInit {

    order: Order;

    constructor(private httpService: HttpService,
                private router: Router,
                private route: ActivatedRoute,) {
    }

    ngOnInit() {
        console.log(this.route.snapshot.params);
        this.httpService.getOrder(this.route.snapshot.params['id']).subscribe((res) => {
            this.order = res['data'].order;
            console.log(this.order);
        });
    }

    back() {
        this.router.navigate(['orders'],{ relativeTo: this.route.parent});
    }

}
