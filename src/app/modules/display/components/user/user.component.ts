import {Component, OnInit} from '@angular/core';
import {User} from '../../../../models';
import {HttpService} from '../../../../core/http.service';
import {ActivatedRoute, Router} from '@angular/router';

@Component({
    selector: 'app-user',
    templateUrl: './user.component.html',
    styleUrls: ['./user.component.less']
})
export class UserComponent implements OnInit {

    user: User;

    constructor(private httpService: HttpService,
                private router: Router,
                private route: ActivatedRoute,) {
    }

    ngOnInit() {
        console.log(this.route.snapshot.params);
        this.httpService.getUser(this.route.snapshot.params['id']).subscribe((res) => {
            this.user = res['data'].user.contact;
            console.log(this.user);
        });
    }

    back() {
        this.router.navigate(['users'],{ relativeTo: this.route.parent});
    }

}
