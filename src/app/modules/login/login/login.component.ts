import {Component, OnInit} from '@angular/core';
import {HttpService} from '../../../core/http.service';
import {AuthService} from '../../../core/auth.service';

@Component({
    selector: 'app-login',
    templateUrl: './login.component.html',
    styleUrls: ['./login.component.less']
})
export class LoginComponent implements OnInit {

    constructor(private httpService: HttpService,
                private authService: AuthService) {
    }

    ngOnInit() {
    }

    login() {
        this.httpService.login({login: 'Supervisor', password: 'Supervisor'})
            .subscribe((res) => {
                  this.authService.token.next(res['data'].token)
                },
                (err) => console.log(err))
    }
}
